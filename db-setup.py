#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  if not os.path.exists('config'):
    os.makedirs('config')
  print("Setting up ratio parameters...")
  print("\n")
  ratio_db = input("ratio db name: ")
  ratio_db_user = input("ratio db user: ")
  mastodon_db = input("Mastodon database: ")
  mastodon_db_user = input("Mastodon database user: ")

  with open(file_path, "w") as text_file:
    print("ratio_db: {}".format(ratio_db), file=text_file)
    print("ratio_db_user: {}".format(ratio_db_user), file=text_file)
    print("mastodon_db: {}".format(mastodon_db), file=text_file)
    print("mastodon_db_user: {}".format(mastodon_db_user), file=text_file)

def create_table(db, db_user, table, sql):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################

# Load configuration from config file
config_filepath = "config/db_config.txt"
mastodon_db = get_parameter("mastodon_db", config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
ratio_db = get_parameter("ratio_db", config_filepath)
ratio_db_user = get_parameter("ratio_db_user", config_filepath)

############################################################
# create database
############################################################

conn = None

try:

  conn = psycopg2.connect(dbname='postgres',
      user=ratio_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  print("Creating database " + ratio_db + ". Please wait...")

  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(ratio_db))
      )
  print("Database " + ratio_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("db_config.txt")
  print("Exiting. Run db-setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("Ratio parameters saved to db-config.txt!")
  print("\n")

############################################################
# Create needed tables
############################################################

print("Creating table...")

########################################

db = ratio_db
db_user = ratio_db_user
table = "federation"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, users int, updated_at timestamptz, software varchar(10), persons INT, bots INT)"
create_table(db, db_user, table, sql)

table =	"federated_servers"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, visited INT, mastodon_servers INT, mastodon_users INT)"
create_table(db, db_user, table, sql)

table = "allinstances"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, users int, updated_at timestamptz, software varchar(10))"
create_table(db, db_user, table, sql)

table = "evo"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, visited INT, mastodon_servers INT, mastodon_users INT)"
create_table(db, db_user, table, sql)

table = "ratio_evo"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, persons INT, bots INT)"
create_table(db, db_user, table, sql)

table = "increase"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, persons INT, bots INT)"
create_table(db, db_user, table, sql)

#####################################

print("Done!")
print("Now you can run setup.py!")
print("\n")
