#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
start_time = time.time()
from six.moves import urllib
from datetime import datetime
from subprocess import call
from mastodon import Mastodon
import threading
import os
import json
import signal
import sys
import os.path
import requests
import operator
import calendar
import psycopg2
from itertools import product

from multiprocessing import Pool, Lock, Process, Queue, current_process
import queue
import multiprocessing

import common

from decimal import *
getcontext().prec = 2

###############################################################################
# INITIALISATION
###############################################################################

def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return True

def download_all(names, time_left, processes):
    args = []
    for i in range(len(names)):
        if not names[i].endswith('--'):
            args.append(names[i])

    pool = multiprocessing.Pool(processes)
    pool_result = pool.imap_unordered(download_one, args)
    timeout_it = timeout_iterator(pool_result, time_left)

    results = []
    try:
        last_print_ts = 0
        for i, rv in enumerate(timeout_it, 1):
            results.append(rv)
            current_ts = int(time.time())
            if current_ts > last_print_ts + 5:
                last_print_ts = current_ts
                print('\r%d of %d done'%(i, len(args)), end='', flush=True)
        print('\r', end='')
    except multiprocessing.context.TimeoutError:
        print(", but no more time left!!!")
    return results

def extend_list(names):

    instances = {}
    new_names = []
    try:
        page = requests.get('https://instances.social/instances.json')
        instances = json.loads(page.content.decode('utf-8'))
        for i in instances:
            if "name" in i:
                beauty = i["name"].strip("/")
                beauty = beauty[beauty.rfind("@")+1:] if beauty.rfind("@") > -1 else beauty
                if beauty.endswith('.'):
                    beauty = beauty[:-1]
                beauty = beauty.encode("idna").decode('utf-8')
                forbidden = beauty + "--"
                if any(s == forbidden for s in names):
                    continue
                new_names.append(beauty)
    except:
        pass
    new_names = sorted(list(set(new_names).union(set(names))))
    return(new_names)

def getall(server):

    global mastodon_servers
    global mastodon_users
    persons = 0
    bots = 0

    try:

        res = requests.get('https://' + server + '/api/v1/instance?',timeout=3)
        res_friendica = requests.get('https://' + server + '/nodeinfo/2.0?',timeout=3)
        check_pix = "compatible; Pixelfed" in res.text
        check_ravenvale = "ravenvale" in res.text
        check_friendica = "friendica" in res_friendica.text
        if check_friendica == False:
            check_friendica = "Friendica" in res_friendica.text

        if (res.ok) and check_pix == False and check_ravenvale == False and check_friendica == False:

            if res.json()['stats']['user_count'] != "":

                nodeinfo = requests.get('https://' + server + '/nodeinfo/2.0.json?',timeout=3)
                check_pleroma = "pleroma" in nodeinfo.text.lower()

                if nodeinfo.status_code == 200 and check_pleroma == False:

                    server_software = "Mastodon"
                    if res.json()['stats']['user_count'] != None:
                        users = res.json()['stats']['user_count']
                    else:
                        users = 0
                    print("***" + server, users)

                    print("\n")


                    ##################################################################################

                    insert_query = """INSERT INTO allinstances(server, users, updated_at, software)
                               VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
                    conn = None

                    try:

                        conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                        cur = conn.cursor()

                        cur.execute(insert_query, (server, users, now, server_software))

                        cur.execute("UPDATE allinstances SET users=(%s), updated_at=(%s), software=(%s) where server=(%s)",
                               (users, now, server_software, server))

                        conn.commit()

                        cur.close()

                    except (Exception, psycopg2.DatabaseError) as error:

                        print(error)

                    finally:

                        if conn is not None:
                            conn.close()

        return

    except KeyError as e:

        pass
        return

    except ValueError as verr:

        pass
        return

    except requests.exceptions.SSLError as errssl:

        pass
        return

    except requests.exceptions.HTTPError as errh:

        pass
        return

    except requests.exceptions.ConnectionError as errc:

        pass
        return

    except requests.exceptions.Timeout as errt:

        pass
        return

    except requests.exceptions.RequestException as err:

        print("5 - OOps: Something Else",err)
        pass

def getserver(server):

    global mastodon_servers
    global mastodon_users
    persons = 0
    bots = 0

    try:

        res = requests.get('https://' + server + '/api/v1/instance?',timeout=3)
        res_friendica = requests.get('https://' + server + '/nodeinfo/2.0?',timeout=3)
        check_pix = "compatible; Pixelfed" in res.text
        check_ravenvale = "ravenvale" in res.text
        check_friendica = "friendica" in res_friendica.text
        if check_friendica == False:
            check_friendica = "Friendica" in res_friendica.text

        if (res.ok) and check_pix == False and check_ravenvale == False and check_friendica == False:

            if res.json()['stats']['user_count'] != "":

                nodeinfo = requests.get('https://' + server + '/nodeinfo/2.0.json?',timeout=3)
                check_pleroma = "pleroma" in nodeinfo.text.lower()

                if nodeinfo.status_code == 200 and check_pleroma == False:

                    server_software = "Mastodon"
                    if res.json()['stats']['user_count'] != None:
                        users = res.json()['stats']['user_count']
                    else:
                        users = 0
                    print(server, users)

                    print("\n")

                    #########################################################################
                    # get server's persons and bots

                    conn = None

                    try:

                        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                        cur = conn.cursor()

                        cur.execute("select count(id) from accounts where actor_type='Person' and domain=(%s)", (server,))

                        row = cur.fetchone()

                        if row != None:

                            persons = row[0]

                        cur.execute("select count(id) from accounts where actor_type is null and domain=(%s)", (server,))

                        row = cur.fetchone()

                        if row != None:

                            persons = persons + row[0]

                        cur.execute("select count(id) from accounts where actor_type='Service' and domain=(%s)", (server,))

                        row = cur.fetchone()

                        if row != None:

                            bots = row[0]

                        cur.close()

                    except (Exception, psycopg2.DatabaseError) as error:

                        print(error)

                    finally:

                        if conn is not None:
                            conn.close()

                    ##################################################################################

                    insert_query = """INSERT INTO federation(server, users, updated_at, software, persons, bots)
                               VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
                    conn = None

                    try:

                        conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                        cur = conn.cursor()

                        cur.execute(insert_query, (server, users, now, server_software, persons, bots))

                        cur.execute("UPDATE federation SET users=(%s), updated_at=(%s), software=(%s), persons=(%s), bots=(%s) where server=(%s)",
                               (users, now, server_software, persons, bots, server))

                        conn.commit()

                        cur.close()

                    except (Exception, psycopg2.DatabaseError) as error:

                        print(error)

                    finally:

                        if conn is not None:
                            conn.close()

        return

    except KeyError as e:

        pass
        return

    except ValueError as verr:

        pass
        return

    except requests.exceptions.SSLError as errssl:

        pass
        return

    except requests.exceptions.HTTPError as errh:

        pass
        return

    except requests.exceptions.ConnectionError as errc:

        pass
        return

    except requests.exceptions.Timeout as errt:

        pass
        return

    except requests.exceptions.RequestException as err:

        print("5 - OOps: Something Else",err)
        pass

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config/config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

# Load language configuration from config file
language_filepath = "ca.txt"  #set english language by changing ca.txt to eng.txt
toot_text1 = get_parameter("toot_text1", language_filepath)
toot_text2 = get_parameter("toot_text2", language_filepath)
toot_text3 = get_parameter("toot_text3", language_filepath)
toot_text4 = get_parameter("toot_text4", language_filepath)
toot_text5 = get_parameter("toot_text5", language_filepath)
toot_text6 = get_parameter("toot_text6", language_filepath)
toot_text7 = get_parameter("toot_text7", language_filepath)
toot_text8 = get_parameter("toot_text8", language_filepath)
toot_text9 = get_parameter("toot_text9", language_filepath)

# Load DB configuration from config file
config_filepath = "config/db_config.txt"
ratio_db = get_parameter("ratio_db", config_filepath)
ratio_db_user = get_parameter("ratio_db_user", config_filepath)
mastodon_db = get_parameter("mastodon_db", config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

###############################################################################
# main

list_file = "list.json"
names = common.get_json(list_file, default_value = [])

if '--generate' in sys.argv:
    extended_names = extend_list(names)
    #with open('list.json', 'w') as f:
    json.dump(extended_names, sys.stdout, indent=4, sort_keys=True)
    #    json.dump(extended_names, f)
    exit(0)

if len(sys.argv) > 1:
    print("Invalid argument, exiting.")
    exit(0)

fediverse_all = extend_list(names)

mastodon_servers = 0
mastodon_users = 0
visited = 0

now = datetime.now()

try:

    conn = None
    conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    ### get all federated servers

    cur.execute("select distinct domain from accounts where domain is not null")

    federated_servers = []
    for row in cur:

        federated_servers.append(row[0])

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

###########################################################################
# get last check values

select_query = "select visited, mastodon_servers, mastodon_users from federated_servers order by datetime desc limit 1"

try:

    conn = None
    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(select_query)

    row = cur.fetchone()

    if row != None:
        visited_before = row[0]
        mastodon_servers_before = row[1]
        mastodon_users_before = row[2]
    else:
        visited_before = 0
        mastodon_servers_before = 0
        mastodon_users_before = 0

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

##############################################################################
# get current federated mastodon servers and users data

nprocs = multiprocessing.cpu_count()
with multiprocessing.Pool(processes=32) as pool:
    pool.starmap(getserver, product(federated_servers))

##############################################################################
# get all fediverse servers data from instances.social

nprocs = multiprocessing.cpu_count()
with multiprocessing.Pool(processes=32) as pool:
    pool.starmap(getall, product(fediverse_all))

##############################################################################
# get totals

servers_query = "select count(server) from federation"
users_query = "select sum(users) from federation"

try:

    conn = None
    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(servers_query)

    row = cur.fetchone()

    if row != None:
        mastodon_servers = row[0]

    cur.execute(users_query)

    row = cur.fetchone()

    if row != None:
        mastodon_users = row[0]

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

##############################################################################

visited = len(federated_servers)

evo_visited = visited - visited_before
evo_servers = mastodon_servers - mastodon_servers_before
evo_users = mastodon_users - mastodon_users_before

################################################################################
#  write data to evo table
################################################################################

insert_query = "INSERT INTO evo(datetime, visited, mastodon_servers, mastodon_users) VALUES(%s,%s,%s,%s)"

conn = None

try:

    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(insert_query, (now, evo_visited, evo_servers, evo_users))

    conn.commit()

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

################################################################################
# get total persons and bots from federated Mastodon servers accounts

total_persons = 0
total_bots = 0

conn = None

try:

    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("select sum(persons) from federation")
    row = cur.fetchone()
    persons = row[0]
    total_persons = total_persons + persons

    cur.execute("select sum(bots) from federation")
    row = cur.fetchone()
    bots = row[0]
    total_bots = total_bots + bots

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

####################################################################################
# get total users from alive servers of https://instances.social/instances.json file

total_persons = 0
total_bots = 0

conn = None

try:

    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("select count(server) from allinstances")
    row = cur.fetchone()
    allservers = row[0]

    cur.execute("select sum(users) from allinstances")
    row = cur.fetchone()
    allusers = row[0]

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

################################################################################
# get users and bots from our Mastodon server

conn = None

try:

    conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("SELECT count(*) FROM users WHERE disabled='f' and approved='t'")
    row = cur.fetchone()
    ownserver_users = row[0]

    cur.execute("select count(id) from accounts where domain is null and actor_type='Person' and id in (SELECT account_id FROM users WHERE disabled='f' and approved='t')")
    row = cur.fetchone()
    own_persons = row[0]
    total_persons = total_persons + own_persons

    cur.execute("select count(id) from accounts where domain is null and actor_type is null and id in (SELECT account_id FROM users WHERE disabled='f' and approved='t')")
    row = cur.fetchone()
    own_persons = own_persons + row[0]
    total_persons = total_persons + own_persons

    cur.execute("select count(id) from accounts where domain is null and actor_type='Service' and id in (SELECT account_id FROM users WHERE disabled='f' and approved='t')")
    row = cur.fetchone()
    own_bots = row[0]
    total_bots = total_bots + own_bots

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

##################################################################################
# add our server persons and bots to federation table

server_software = "Mastodon"
insert_query = """INSERT INTO federation(server, users, updated_at, software, persons, bots)
                               VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
conn = None

try:

    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(insert_query, (mastodon_hostname, ownserver_users, now, server_software, own_persons, own_bots))

    cur.execute("UPDATE federation SET users=(%s), updated_at=(%s), software=(%s), persons=(%s), bots=(%s) where server=(%s)",
               (ownserver_users, now, server_software, own_persons, own_bots, mastodon_hostname))

    conn.commit()

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

################################################################################
# Write to database current totals

insert_query = "INSERT INTO ratio_evo(datetime, persons, bots) VALUES(%s,%s,%s)"

conn = None

try:

    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(insert_query, (now, total_persons, total_bots))

    conn.commit()

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

###############################################################################
# write increases to increase table

conn = None

try:

    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("select datetime, persons - lag(persons) over (order by datetime), bots - lag(bots) over (order by datetime) as increase from ratio_evo")

    rows = cur.fetchall()

    for row in rows:

        insert_query = "INSERT INTO increase(datetime, persons, bots) VALUES(%s,%s,%s) ON CONFLICT DO NOTHING"

        cur.execute(insert_query, (row[0], row[1], row[2]))
        #cur.execute("update increase set persons=(%s), bots=(%s) where datetime=row[0]")

        conn.commit()

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

################################################################################
# Write total federated servers, visited, mastodon servers and users to database
#

insert_query = "INSERT INTO federated_servers(datetime, visited, mastodon_servers, mastodon_users) VALUES(%s,%s,%s,%s)"
conn = None

try:

    conn = psycopg2.connect(database = ratio_db, user = ratio_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(insert_query, (now, visited, mastodon_servers, mastodon_users))

    conn.commit()

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()


###############################################################################
# T  O  O  T !
###############################################################################

federated_accounts = total_persons + total_bots
perc_persons = str(round((total_persons * 100)/ federated_accounts, 2))
perc_bots = str(round((total_bots * 100)/ federated_accounts, 2))
exec_time = str(round((time.time() - start_time), 2))

toot_text = toot_text1 + ": " + str(allservers) + " \n"
toot_text += toot_text2 + ": " + str(allusers)+ " \n"
toot_text += "\n"
toot_text += toot_text3 + " \n"
toot_text += "\n"
toot_text += toot_text4 + ": " + str(visited) + " \n"
toot_text += toot_text5 + ": " + str(mastodon_servers) + "\n"
toot_text += "\n"
toot_text += toot_text6 + ": " + str(federated_accounts) + "\n"
toot_text += "\n"
toot_text += toot_text7 + ": " + str(total_persons) + " (" + str(perc_persons) + "%)" + "\n"
toot_text += toot_text8 + ": " + str(total_bots) + " (" + str(perc_bots) + "%)" + "\n"
toot_text += "\n"
toot_text += toot_text9  % exec_time

print(toot_text)

mastodon.status_post(toot_text, in_reply_to_id=None, )

