# ratio
Check & toot how many of your Mastodon server federated Mastodon accounts are persons and how many are bots

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon's admin account

### Usage:

Within Python Virtual Environment:

1. Run 'python db-setup.py' to setup your Mastodon server DB paramteres and new database and tables for Grafana.

2. Run 'python setup.py' to get your bot's access token of your Mastodon server existing account. It will be saved to 'secrets/secrets.txt' for further use.

3. Run 'python ratio.py' to check your federated Mastodon servers and get their persons and bots data.

4. Use your favourite scheduling method to set ratio.py to run regularly. 

Note: install all needed packages with 'pip install package' or use 'pip install -r requirements.txt' to install them. 


**New Feature**
11.05.2020 - Added check and count all users of alive servers from [instances.social list] (https://instances.social/instances.json)
